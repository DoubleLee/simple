#pragma once
#include "Simple/Component.hpp"
#include <memory>
#include "Simple/CoolDown.hpp"

namespace Simple
{
class RigidBody;
class GameObject;
}
class PlayerShip : public Simple::Component
{
public:
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void UpdatePhysics() override;
	void UpdateLate() override;
	void OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;
	void OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;
	
	PlayerShip();
	~PlayerShip();

protected:
	weak_ptr<Simple::RigidBody> wpRigidBody;

	float linearForce;
	float angularForce;
	Simple::CoolDown fireRate;
};

