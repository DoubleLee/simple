#include "SpaceShooterReloaded.hpp"

#include "Simple/Scene.hpp"
#include "Simple/Component.hpp"

#include "Box2D/Dynamics/b2World.h"

#include "PlayerShip.hpp"
#include "Projectile.hpp"

SpaceShooterReloaded::SpaceShooterReloaded()
	:
	Game(sf::VideoMode(1280, 720), "Space Shooter Reloaded!")
	{
	}


SpaceShooterReloaded::~SpaceShooterReloaded()
	{
	}

void SpaceShooterReloaded::OnSceneLoaded()
	{
	GetCurrentScene()->GetWorld()->SetGravity(b2Vec2_zero);
	}

unique_ptr<Simple::Component> SpaceShooterReloaded::LoadGameComponent(Simple::GameObject* pParent, tinyxml2::XMLElement* pComponent, const string& typeString)
	{
	unique_ptr<Simple::Component> pComp;

	if ( typeString == "PlayerShip" )
		{
		pComp = make_unique<PlayerShip>();
		}
	else if ( typeString == "Projectile" )
		{
		pComp = make_unique<Projectile>();
		}
	
	return pComp;
	}
