#pragma once
#include <Simple/Game.hpp>

class SpaceShooterReloaded : public Simple::Game
{
public:
	void OnSceneLoaded() override;
private:
	unique_ptr<Simple::Component> LoadGameComponent(Simple::GameObject* pParent, tinyxml2::XMLElement* pComponent, const string& typeString) override;
public:
	SpaceShooterReloaded();
	~SpaceShooterReloaded();
};

