#include "PlayerShip.hpp"

#include "SpaceShooterReloaded.hpp"

#include "Simple/Engine.hpp"

#include "SFML/Window/Keyboard.hpp"

void PlayerShip::Constructor()
	{
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	}

void PlayerShip::Initialize()
	{
	}

void PlayerShip::UpdateEarly()
	{
	}

void PlayerShip::UpdatePhysics()
	{
	auto spRigidBody = wpRigidBody.lock();

	b2Vec2 forceDir(0,0);
	float rotateRadians = 0;

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W) )
		{
		forceDir.y += -1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) )
		{
		forceDir.y += 1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A) )
		{
		forceDir.x += -1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) )
		{
		forceDir.x += 1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q) )
		{
		rotateRadians += -1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::E) )
		{
		rotateRadians += 1;
		}

	if ( sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && fireRate.HasCooledDown(Simple::Game::GetTimeSinceGameLoad()))
		{
		auto pPrefab = Simple::Game::GetGame()->GetCurrentScene()->InstantiatePrefab("Projectile.prefab");
		pPrefab->GetComponent<Simple::RigidBody>().lock()->GetBody()->SetTransform( spRigidBody->GetBody()->GetWorldPoint(b2Vec2(0, -1)), spRigidBody->GetBody()->GetAngle() );
		}

	forceDir.Normalize();

	b2Vec2 transformedForce = spRigidBody->GetBody()->GetWorldVector( forceDir );
	
	transformedForce *= linearForce * Simple::Game::GetPhysicsTimeStep().asSeconds();

	spRigidBody->GetBody()->ApplyTorque(rotateRadians * angularForce * Simple::Game::GetPhysicsTimeStep().asSeconds(), true);
	spRigidBody->GetBody()->ApplyForceToCenter(transformedForce, true);
	}

void PlayerShip::UpdateLate()
	{
	}

void PlayerShip::OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	}

void PlayerShip::OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	}

void PlayerShip::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("LinearForce", linearForce);
	pComponentEle->SetAttribute("AngularForce", angularForce);
	}

void PlayerShip::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	pComponentEle->QueryFloatAttribute("LinearForce", &linearForce);
	pComponentEle->QueryFloatAttribute("AngularForce", &angularForce);
	}

PlayerShip::PlayerShip()
	:
	Component(),
	linearForce(1.0f),
	angularForce(1.0f),
	fireRate(sf::milliseconds(500))
	{

	}


PlayerShip::~PlayerShip()
	{
	}
