#include "Projectile.hpp"

#include "Simple/Engine.hpp"

void Projectile::Constructor()
	{
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	}

void Projectile::Initialize()
	{
	
	}

void Projectile::UpdateEarly()
	{
	auto spRigidBody = wpRigidBody.lock();
	spRigidBody->GetBody()->SetLinearVelocity( spRigidBody->GetBody()->GetWorldVector( b2Vec2(0,-10) ) );
	}

void Projectile::Update()
	{

	}

void Projectile::UpdateLate()
	{

	}

void Projectile::UpdatePhysics()
	{
	}

void Projectile::OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	}

void Projectile::OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	}

void Projectile::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	}

void Projectile::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	}

Projectile::Projectile()
	{
	}


Projectile::~Projectile()
	{
	}
