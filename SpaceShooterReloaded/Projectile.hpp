#pragma once
#include <Simple/Component.hpp>


class Projectile : public Simple::Component
{
public:
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;
	void UpdatePhysics() override;
	void OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;
	void OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;
	
	Projectile();
	~Projectile();


protected:
	weak_ptr<Simple::RigidBody> wpRigidBody;
};

