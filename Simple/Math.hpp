#pragma once


namespace Simple
{
namespace Math
	{
	
	float MetersToPixels(float meters);

	float PixelsToMeters(float pixels);

	int GetRandomBetween(int low, int high);

	void SetSeed(unsigned long long seed);
	void SetSeedByTime();
	}
}
