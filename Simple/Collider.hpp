#pragma once

#include "Component.hpp"

#include <Box2D/Dynamics/b2Fixture.h>
#include <Box2D/Dynamics/Contacts/b2Contact.h>

namespace Simple
{
class RigidBody;
class Sprite;

class Collider : public Component
	{
	friend RigidBody;
	public:
		explicit Collider();
		~Collider();

		void Constructor() override;
		void SetFixture(b2Fixture * pFixure);
		b2Fixture * GetFixture() const;
	private:
		b2Fixture * pFixture;

	protected:
		weak_ptr<RigidBody> wpBody;
		weak_ptr<Sprite> wpSprite;
	};

}