#include "Collider.hpp"

#include "GameObject.hpp"
#include "RigidBody.hpp"
#include "Sprite.hpp"

namespace Simple
{

Collider::Collider()
	:
	Component(),
	pFixture(nullptr)
	{
	
	}

Collider::~Collider()
	{

	}

void Collider::Constructor()
	{
	wpBody = GetParent()->GetComponent<RigidBody>();
	wpSprite = GetParent()->GetComponent<Sprite>();
	}

void Collider::SetFixture(b2Fixture* pFixture)
	{
	if (pFixture != nullptr)
		{
		if ( this->pFixture == nullptr )
			{
			this->pFixture = pFixture;
			}
		else
			{
			throw runtime_error("Tried to set a colliders fixture when one already existed.");
			}
		}
	else
		{
		throw runtime_error("Tried to set a colliders fixture to nullptr.");
		}
		
	}


b2Fixture* Collider::GetFixture() const
	{
	if ( pFixture )
		{
		return pFixture;
		}
	else
		throw runtime_error("Colliders fixture was null.");
	}

}