#include "Math.hpp"

#include <chrono>
#include <random>

namespace Simple
{
namespace Math
{
const float pixelsPerMeter = 60.0f;
std::mt19937_64 generator;

float MetersToPixels(float meters)
	{
	return meters * pixelsPerMeter;
	}

float PixelsToMeters(float pixels)
	{
	return pixels / pixelsPerMeter;
	}

int GetRandomBetween(int low, int high)
	{
	std::uniform_int_distribution<int> dist(low, high);
	return dist(generator);
	}

void SetSeed(unsigned long long seed)
	{
	generator.seed(seed);
	}

void SetSeedByTime()
	{
	auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	SetSeed(seed);
	}
	}
}