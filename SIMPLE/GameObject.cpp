#include "GameObject.hpp"

#include "Component.hpp"
#include "Game.hpp"

#include "tinyxml2.hpp"
#include "Collider.hpp"
using namespace tinyxml2;

namespace Simple
{
GameObject::GameObject()
	:
	taggedForDelete(false),
	constructed(false),
	initialized(false)
	{

	}

GameObject::~GameObject()
	{

	}

void GameObject::SaveToXML(XMLElement* pGameObjectsEle, XMLDocument& doc)
	{
	XMLElement * pGameObjectEle = doc.NewElement("GameObject");
	pGameObjectsEle->InsertEndChild(pGameObjectEle);

	pGameObjectEle->SetAttribute("Tag", GetTag().c_str());

	XMLElement * pComponentsEle = doc.NewElement("Components");
	pGameObjectEle->InsertEndChild(pComponentsEle);

	for ( size_t i = 0; i < components.size(); ++i )
		{
		XMLElement * pComponentEle = doc.NewElement(components[i]->GetName().c_str());
		pComponentsEle->InsertEndChild(pComponentEle);
		components[i]->SaveToXML(pComponentEle, doc);
		}
	}

	void GameObject::LoadFromXML(tinyxml2::XMLElement* pGameObject)
		{
		SetTag(string(pGameObject->Attribute("Tag")));
		XMLElement * pComponents = pGameObject->FirstChildElement("Components");
		XMLElement * pComponent = pComponents->FirstChildElement();

		while( pComponent )
			{
			components.emplace_back(Game::GetGame()->LoadEngineComponent(this, pComponent));
			pComponent = pComponent->NextSiblingElement();
			}
		}

void GameObject::Constructor()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Constructor();
		}
	}

void GameObject::Initialize()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Initialize();
		}
	}


void GameObject::UpdateEarly()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->UpdateEarly();
		}
	}

void GameObject::Update()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Update();
		}
	}

void GameObject::UpdateLate()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->UpdateLate();
		}
	}

void GameObject::UpdatePhysics()
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->UpdatePhysics();
		}
	}

void GameObject::OnCollisionEnter(b2Contact* pContact, Collider * pThisCollider, Collider * pOtherCollider)
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->OnCollisionEnter(pContact, pThisCollider, pOtherCollider);
		}
	}

	void GameObject::OnCollisionStay(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
		{
		const auto pFirstObject = components.data();
		const auto pEndArray = pFirstObject + components.size();
		for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
			{
			(*pIndex)->OnCollisionStay(pContact, pThisCollider, pOtherCollider);
			}
		}

	void GameObject::OnCollisionExit(b2Contact* pContact, Collider * pThisCollider, Collider * pOtherCollider)
	{
	const auto pFirstObject = components.data();
	const auto pEndArray = pFirstObject + components.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->OnCollisionExit(pContact, pThisCollider, pOtherCollider);
		}
	}

const string& GameObject::GetTag() const
	{
	return tag;
	}

void GameObject::SetTag(const string& tag)
	{
	this->tag = tag;
	}

bool GameObject::TagEquals(const string & otherTag) const
	{
	return std::strcmp(this->tag.c_str(), otherTag.c_str()) == 0;
	}

	bool GameObject::GetTaggedForDelete() const
		{
		return taggedForDelete;
		}

	void GameObject::SetTaggedForDelete(bool deleteThis)
		{
		taggedForDelete = deleteThis;
		}
	bool GameObject::isConstructed() const
		{
		return constructed;
		}

	bool GameObject::isInitialized() const
		{
		return initialized;
		}

	void GameObject::SetConstructed()
		{
		constructed = true;
		}

	void GameObject::SetInitialized()
		{
		initialized = true;
		}
	}