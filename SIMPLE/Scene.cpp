#include "Scene.hpp"

#include <fstream>
#include <iostream>
#include <stdexcept>

#include "Texture.hpp"
#include "SoundBuffer.hpp"
#include "GameObject.hpp"
#include "Game.hpp"
#include "Sprite.hpp"
#include "AnimatedSprite.hpp"
#include "Collider.hpp"
#include "RigidBody.hpp"
#include "Prefab.hpp"

#include <Box2D/Dynamics/b2World.h>
#include <Box2D/Dynamics/Contacts/b2Contact.h>

#include "SFML/Graphics/Rect.hpp"
#include "SFML/System/Vector2.hpp"

#include "tinyxml2.hpp"
using namespace tinyxml2;

namespace Simple
{
Scene::Scene()
	:
	resortSprites(false),
	filePath(),
	pWorld(make_unique<b2World>(b2Vec2(0.0f, 9.8f))),
	textures(),
	soundBuffers(),
	prefabs(),
	sceneSprites(),
	gameObjects(),
	physicsTimeStep( sf::seconds(1) / 120.0f ),
	lastPhysicsUpdate()
	{
	pWorld->SetAllowSleeping(true);
	pWorld->SetContactListener(this);
	}

Scene::~Scene()
	{

	}

const string& Scene::GetFilePath() const
	{
	return filePath;
	}

void Scene::Update()
	{
	CombineAddedGameObjects();
	// call UpdateEarly
	const auto pFirstObject = gameObjects.data();
	const auto pEndArray = pFirstObject + gameObjects.size();

	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->UpdateEarly();
		}

	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Update();
		}

	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->UpdateLate();
		}

	UpdateCollisionStay();

	UpdatePhysics();

	EraseTaggedGameObjects();
	}

bool SortSprites( const Sprite * sprite1, const Sprite * sprite2 )
	{
	bool isLayerLessThan = sprite1->GetLayer() < sprite2->GetLayer();
	if ( isLayerLessThan )
		return true;
	
	bool isLayerMoreThan = sprite1->GetLayer() > sprite2->GetLayer();
	if ( isLayerMoreThan )
		return false;

	return (sprite1->getTexture() < sprite2->getTexture());
	}

bool SortSpritesByTexture( const Sprite* sprite1, const Sprite * sprite2 )
	{
	return (sprite1->getTexture() < sprite2->getTexture());
	}

void Scene::Draw()
	{
	/*
	// This code can print the vector of sorted sprites. It will print it every single frame though
	ofstream output("Organized Drawing.txt");
	output.exceptions(ios::badbit | ios::failbit);
	for (int i = 0; i < sceneSprites.size(); ++i )
		{
		output << sceneSprites[i]->GetLayer() << ' ' << static_cast<const Texture*>(sceneSprites[i]->getTexture())->GetFilePath() << std::endl;
		}
	*/
	// a Sprite has changed texture or layer so the array needs to be resorted.
	if ( resortSprites )
		{
		std::sort(sceneSprites.begin(), sceneSprites.end(), SortSprites);
		resortSprites = false;
		}

	// this draws the sorted array of Simple::Sprites.
	Game * pGame = Game::GetGame();

	auto windowRect = sf::FloatRect(Game::GetGame()->getViewport(Game::GetGame()->getView()));
	sf::FloatRect spriteRect;

	// DRAW CODE
	const auto pFirstObject = sceneSprites.data();
	const auto pEndArray = pFirstObject + sceneSprites.size();

	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		// only draw if it's aabb is in the viewport.
		spriteRect = (*pIndex)->getGlobalBounds();
		if ( windowRect.intersects(spriteRect) )
			{
			pGame->draw(*(*pIndex));
			}
		}
	}



void Scene::Construct()
	{
	const auto pFirstObject = gameObjects.data();
	const auto pEndArray = pFirstObject + gameObjects.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Constructor();
		}
	}

void Scene::Initialize()
	{
	const auto pFirstObject = gameObjects.data();
	const auto pEndArray = pFirstObject + gameObjects.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		(*pIndex)->Initialize();
		}
	}

Texture & Scene::GetTextureBy(int index)
	{
	return (*textures.at(index));
	}

Texture & Scene::GetTextureBy(const string & filePath)
	{
	for ( size_t i = 0; i < textures.size(); ++i )
		{
		if ( textures[i]->GetFilePath() == filePath )
			{
			return (*textures[i].get());
			}
		}

	// wasn't found try loading it.
	LoadTexture(filePath);
	return *textures.back();
	}

int Scene::GetTextureIndexBy(const sf::Texture * texture)
	{
	for ( size_t i = 0; i < textures.size(); ++i )
		{
		if ( textures[i].get() == texture )
			{
			return i;
			}
		}

	throw invalid_argument("Texture was not found in textures vector.");
	}

SoundBuffer & Scene::GetSoundBufferBy(int index)
	{
	return (*soundBuffers.at(index));
	}

const SoundBuffer & Scene::GetSoundBufferBy(const string & filePath)
	{
	for ( size_t i = 0; i < soundBuffers.size(); ++i )
		{
		if ( soundBuffers[i]->GetFilePath() == filePath )
			{
			return (*soundBuffers[i].get());
			}
		}

	// wasn't found try loading it.
	LoadSoundBuffer(filePath);
	return *soundBuffers.back();
	}

int Scene::GetSoundIndexBy(const sf::SoundBuffer * soundBuffer)
	{
	for ( size_t i = 0; i < soundBuffers.size(); ++i )
		{
		if ( soundBuffers[i].get() == soundBuffer )
			{
			return i;
			}
		}

	throw invalid_argument("SoundBuffer was not found in soundBuffer vector.");
	}

	void Scene::LoadTexture(const string & filePath)
		{
		auto pTexture = make_unique<Texture>();
		pTexture->LoadFromFile(filePath);
		textures.emplace_back(std::move(pTexture));
		}

	void Scene::LoadSoundBuffer(const string & filePath)
		{
		auto pSoundBuffer = make_unique<SoundBuffer>();
		pSoundBuffer->LoadFromFile(filePath);
		soundBuffers.emplace_back(std::move(pSoundBuffer));
		}


void Scene::LoadScene(const string & filePath)
	{
	tinyxml2::XMLDocument doc;
	auto result = doc.LoadFile(filePath.c_str());
	if ( result == tinyxml2::XML_SUCCESS )
		{
		XMLElement * pRoot = doc.FirstChildElement();

		XMLElement * pTextures = pRoot->FirstChildElement("Textures");

		XMLElement * pTexture = pTextures->FirstChildElement("Texture");

		string filePath;
		while ( pTexture )
			{
			filePath = pTexture->Attribute("FilePath");
			LoadTexture(filePath);
			pTexture = pTexture->NextSiblingElement();
			}

		XMLElement * pSounds = pRoot->FirstChildElement("Sounds");
		XMLElement * pSound = pSounds->FirstChildElement("Sound");

		while(pSound)
			{
			filePath = pSound->Attribute("FilePath");
			LoadSoundBuffer(filePath);
			pSound = pSound->NextSiblingElement("Sound");
			}

		XMLElement * pPrefabs = pRoot->FirstChildElement("Prefabs");
		XMLElement * pPrefab = pPrefabs->FirstChildElement("Prefab");

		while ( pPrefab )
			{
			filePath = pPrefab->Attribute("FilePath");
			auto upPrefab = make_unique<Prefab>(filePath);
			prefabs.emplace_back(std::move(upPrefab));
			pPrefab = pPrefab->NextSiblingElement("Prefab");
			}

		XMLElement * pGameObjects = pRoot->FirstChildElement("GameObjects");
		XMLElement * pGameObject = pGameObjects->FirstChildElement("GameObject");

		while(pGameObject)
			{
			LoadGameObject(pGameObject, true);

			pGameObject = pGameObject->NextSiblingElement("GameObject");
			}
		}
	else
		throw runtime_error("Could not load xml file, " + filePath + " " + to_string(result));
	}

void Scene::SaveScene(const string & filePath)
	{
	tinyxml2::XMLDocument doc;

	using namespace tinyxml2;
	XMLElement * pRoot = doc.NewElement("Scene");
	doc.InsertFirstChild(pRoot);

	XMLElement * pTexturesEle = doc.NewElement("Textures");
	pRoot->InsertEndChild(pTexturesEle);
	
	for ( size_t i = 0; i < textures.size(); ++i )
		{
		XMLElement * pTextureEle = doc.NewElement("Texture");
		pTexturesEle->InsertEndChild(pTextureEle);
		pTextureEle->SetAttribute("FilePath", textures[i]->GetFilePath().c_str());
		}

	XMLElement * pSoundsEle = doc.NewElement("Sounds");
	pRoot->InsertEndChild(pSoundsEle);

	for (size_t i = 0; i < soundBuffers.size(); ++i)
		{
		XMLElement * pSoundEle = doc.NewElement("Sound");
		pSoundsEle->InsertEndChild(pSoundEle);
		pSoundEle->SetAttribute("FilePath", soundBuffers[i]->GetFilePath().c_str());
		}

	XMLElement * pGameObjectsEle = doc.NewElement("GameObjects");
	pRoot->InsertEndChild(pGameObjectsEle);
	for (size_t i = 0; i < gameObjects.size(); ++i)
		{
		gameObjects[i]->SaveToXML(pGameObjectsEle, doc);
		}

	auto result = doc.SaveFile(filePath.c_str());

	if ( result != tinyxml2::XML_SUCCESS )
		{
		throw runtime_error("Failed to save scene file, " + filePath);
		}
	}

weak_ptr<GameObject> Scene::GetGameObjectBy(const string& tag)
	{
	const auto pFirstObject = gameObjects.data();
	const auto pEndArray = pFirstObject + gameObjects.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		if ((*pIndex)->TagEquals(tag))
			{
			return (*pIndex);
			}
		}

	return weak_ptr<GameObject>();
	}

vector<weak_ptr<GameObject>> Scene::GetGameObjectsBy(const string& tag)
	{
	vector<weak_ptr<GameObject>> result;

	const auto pFirstObject = gameObjects.data();
	const auto pEndArray = pFirstObject + gameObjects.size();
	for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
		{
		if ( (*pIndex)->TagEquals(tag) )
			result.emplace_back((*pIndex));
		}

	return std::move(result);
	}

	void Scene::BeginContact(b2Contact* pContact)
		{
		// TODO: OnCollisionEnter(); also consider OnCollisionStay()
		auto compA = static_cast<Collider*>(pContact->GetFixtureA()->GetUserData());
		auto compB = static_cast<Collider*>(pContact->GetFixtureB()->GetUserData());

		auto gameObjectA = compA->GetParent();
		auto gameObjectB = compB->GetParent();

		gameObjectA->OnCollisionEnter(pContact, compA, compB);
		gameObjectB->OnCollisionEnter(pContact, compB, compA);
		}

	void Scene::StayContact(b2Contact * pContact)
		{
		auto compA = static_cast<Collider*>(pContact->GetFixtureA()->GetUserData());
		auto compB = static_cast<Collider*>(pContact->GetFixtureB()->GetUserData());

		auto gameObjectA = compA->GetParent();
		auto gameObjectB = compB->GetParent();

		gameObjectA->OnCollisionStay(pContact, compA, compB);
		gameObjectB->OnCollisionStay(pContact, compB, compA);
		}

	void Scene::EndContact(b2Contact* pContact)
		{
		// TODO: OnCollisionExit();
		auto compA = static_cast<Collider*>(pContact->GetFixtureA()->GetUserData());
		auto compB = static_cast<Collider*>(pContact->GetFixtureB()->GetUserData());

		auto gameObjectA = compA->GetParent();
		auto gameObjectB = compB->GetParent();

		gameObjectA->OnCollisionExit(pContact, compA, compB);
		gameObjectB->OnCollisionExit(pContact, compB, compA);
		}

	void Scene::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
		{
		}

	void Scene::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
		{
		}

	const sf::Time& Scene::GetPhysicsTimeStep() const
		{
		return physicsTimeStep;
		}

	// this method exists so we can avoid searching for the Sprite in the scene when we want
	// to draw the scene.
	void Scene::AddSprite(Sprite* pSprite)
		{
		sceneSprites.push_back(pSprite);
		}
	// obviously we need to remove the a sprite from the array when it's destructor is called
	// the Simple::Sprite destructor must always call this.
	void Scene::RemoveSprite(Sprite* pSprite)
		{
		auto end = sceneSprites.end();

		for ( auto iter = sceneSprites.begin(); iter != end; ++iter)
			{
			if ( *iter == pSprite )
				{
				sceneSprites.erase(iter);
				return;
				}
			}
		}

shared_ptr<GameObject> Scene::InstantiatePrefab(const string & filePath)
		{
		Prefab * pPrefab = nullptr;

		for (size_t i = 0; i < prefabs.size(); ++i)
			{
			if (prefabs[i]->GetFilePath() == filePath)
				{
				pPrefab = prefabs[i].get();
				break;
				}
			}

		// if prefab wasn't found try loading it.
		if ( !pPrefab )
			{
			prefabs.emplace_back(std::make_unique<Prefab>(filePath));
			pPrefab = prefabs.back().get();
			}

		// Tell the scene to instantiate the prefab.
		if ( pPrefab )
			{
			auto pGameObject = LoadGameObject(pPrefab->GetGameObjectElement());
			pGameObject->Constructor();
			pGameObject->SetConstructed();
			pGameObject->Initialize();
			pGameObject->SetInitialized();
			return std::move(pGameObject);
			}
		else
			{
			throw invalid_argument("The requested prefab was not found, " + filePath);
			}
			
		}

shared_ptr<GameObject> Scene::LoadGameObject(tinyxml2::XMLElement* pGameObject, bool directIntoMain)
		{
		auto & gameObjectVector = directIntoMain ? gameObjects : addedGameObjects;
		gameObjectVector.emplace_back(make_shared<GameObject>());
		gameObjectVector.back()->LoadFromXML(pGameObject);
		return gameObjectVector.back();
		}

	void Scene::EraseTaggedGameObjects()
		{
		for ( auto iter = gameObjects.begin(); iter != gameObjects.end(); )
			{
			if ( (*iter)->GetTaggedForDelete() )
				iter = gameObjects.erase(iter);
			else
				++iter;
			}
		}

	void Scene::CombineAddedGameObjects()
		{
		// call construct on all added game objects
		for (size_t i = 0; i < addedGameObjects.size(); ++i)
			{
			if ( !addedGameObjects[i]->isConstructed() )
				{
				addedGameObjects[i]->Constructor();
				addedGameObjects[i]->SetConstructed();
				}
			}
		// then call initialize on all added game objects
		for (size_t i = 0; i < addedGameObjects.size(); ++i)
			{
			if ( !addedGameObjects[i]->isInitialized() )
				{
				addedGameObjects[i]->Initialize();
				addedGameObjects[i]->SetInitialized();
				}
			}
		// the move them over to the main vector.
		while(addedGameObjects.size())
			{
			gameObjects.emplace_back(addedGameObjects.back());
			addedGameObjects.pop_back();
			}
		}

	void Scene::UpdatePhysics()
		{
		const auto pFirstObject = gameObjects.data();
		const auto pEndArray = pFirstObject + gameObjects.size();
		if ( (Game::GetTimeSinceGameLoad() - lastPhysicsUpdate) >= physicsTimeStep )
			{
			// Call the UpdatePhysics on each game object b4 step, so the step has the new data
			for( auto pIndex = pFirstObject; pIndex < pEndArray; ++pIndex )
				{
				(*pIndex)->UpdatePhysics();
				}

			pWorld->Step(physicsTimeStep.asSeconds(), 8, 3);
			lastPhysicsUpdate = Game::GetTimeSinceGameLoad();
			b2Body * pBody = pWorld->GetBodyList();

			while (pBody)
				{
				static_cast<RigidBody*>(pBody->GetUserData())->UpdateSprite();
				pBody = pBody->GetNext();
				}
			}
		}

	void Scene::UpdateCollisionStay()
		{
		auto pContact = pWorld->GetContactList();

		while (pContact)
			{
			if ( pContact->IsTouching() )
				{
				StayContact(pContact);
				}
			pContact = pContact->GetNext();
			}
		}

	void Scene::SetResortSprites()
	{
	resortSprites = true;
	}

	b2World * Scene::GetWorld() const
	{
	return pWorld.get();
	}
}