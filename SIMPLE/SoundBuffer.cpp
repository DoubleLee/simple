#include "SoundBuffer.hpp"

namespace Simple
{

SoundBuffer::SoundBuffer()
	{
	}


SoundBuffer::~SoundBuffer()
	{
	}

void SoundBuffer::LoadFromFile(const std::string& filePath)
	{
	if (loadFromFile(filePath))
		{
		this->filePath = filePath;
		}
	else
		throw std::invalid_argument("SoundBuffer file not found or unreadable, " + filePath);
	}

const std::string& SoundBuffer::GetFilePath() const
	{
	return filePath;
	}
}