#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include "Component.hpp"

namespace Simple
{
class Sprite : public sf::Sprite, public Component
{
public:
	void Constructor() override;
	explicit Sprite();
	virtual ~Sprite();

	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponent) override;

	int GetLayer() const;
	void SetLayer(int layer);
	void setTexture(sf::Texture& texture);
protected:
	int layer;
};

}