#pragma once
#include <string>

namespace Simple
{
namespace Utilities
{
std::string GetClassName(const type_info & typeInfo);
}

}