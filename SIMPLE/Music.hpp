#pragma once

#include <SFML\Audio\Music.hpp>
#include "Component.hpp"

namespace Simple
{
class GameObject;

class Music : public sf::Music, public Component
	{
public:
	explicit Music();
	~Music();

	void Constructor() override;
	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponentEle) override;

private:
	string musicFile;
	bool autoPlay;
	bool loop;
	};
}
