#include "Logger.hpp"
#include <chrono>
#include <ctime>
#include <string>

namespace Simple
{
Logger::Logger()
	{
	logStream.exceptions(std::ios::failbit | std::ios::badbit );
	logStream.open("LogFile.txt");
	}


Logger::~Logger()
	{
	logStream.flush();
	logStream.close();
	}

	std::ofstream & Logger::Log(const std::string & data)
		{
		logStream << GetTime() << ' ' << data << std::endl;
		return logStream;
		}

	const char * Logger::GetTime()
		{
		auto time = std::chrono::high_resolution_clock::to_time_t(std::chrono::high_resolution_clock::now() );

		return std::ctime(&time);
		}

	}