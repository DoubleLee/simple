#pragma once
#include <SFML/System/Time.hpp>

namespace Simple
{

class CoolDown
{
public:
	CoolDown(sf::Time coolDownAmount);
	~CoolDown();

	bool HasCooledDown(sf::Time timeSinceLoad);

private:
	sf::Time coolDownAmount;
	sf::Time nextCoolDownTrigger;
};

}