#include "Sprite.hpp"

#include "Game.hpp"
#include "Scene.hpp"
#include "Texture.hpp"

#include "tinyxml2.hpp"

namespace Simple
{
void Sprite::Constructor()
	{
	Game::GetGame()->GetCurrentScene()->AddSprite(this);
	}

Sprite::Sprite()
	:
	sf::Sprite(),
	Component(),
	layer(0)
	{
	
	}

Sprite::~Sprite()
	{
	Game::GetGame()->GetCurrentScene()->RemoveSprite(this);
	}

void Sprite::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	int textureIndex = Game::GetGame()->GetCurrentScene()->GetTextureIndexBy(getTexture());
	const sf::Vector2f & pos = getPosition();
	float angle = getRotation();
	auto bounds = getTextureRect();
	auto scales = getScale();
	// these values may need to replace bounds. values below.
	//float width = float(bounds.width) * scales.x;
	//float height = float(bounds.height) * scales.y;
	pComponentEle->SetAttribute("TexIndex", textureIndex);
	pComponentEle->SetAttribute("Layer", layer);
	pComponentEle->SetAttribute("PosX", pos.x);
	pComponentEle->SetAttribute("PosY", pos.y);
	pComponentEle->SetAttribute("SizeX", bounds.width);
	pComponentEle->SetAttribute("SizeY", bounds.height);
	pComponentEle->SetAttribute("Angle", angle);
	}

void Sprite::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	int texIndex = -1;
	pComponent->QueryIntAttribute("TexIndex", &texIndex);
	if ( texIndex > -1 )
		{
		setTexture(Game::GetGame()->GetCurrentScene()->GetTextureBy(texIndex));
		}
	else
		{
		string filePath = pComponent->Attribute("TexFilePath");
		setTexture(Game::GetGame()->GetCurrentScene()->GetTextureBy(filePath));
		}

	pComponent->QueryIntAttribute("Layer", &layer);
	
	sf::Vector2f pos;
	pComponent->QueryFloatAttribute("PosX", &pos.x);
	pComponent->QueryFloatAttribute("PosY", &pos.y);
	sf::Vector2f size;
	pComponent->QueryFloatAttribute("SizeX", &size.x);
	pComponent->QueryFloatAttribute("SizeY", &size.y);
	auto bounds = this->getTextureRect();
	setScale(size.x / float(bounds.width), size.y / float(bounds.height));
	auto scale = getScale();
	setOrigin((float(bounds.width) / 2.0f), (float(bounds.height) / 2.0f));
	
	float angle = 0;
	pComponent->QueryFloatAttribute("Angle", &angle);
	setPosition(pos);
	setRotation(angle);
	}

int Sprite::GetLayer() const
	{
	return layer;
	}

void Sprite::SetLayer(int layer)
	{
	if ( this->layer != layer )
		{
		this->layer = layer;
		Game::GetGame()->GetCurrentScene()->SetResortSprites();
		}
	}

	void Sprite::setTexture(sf::Texture& texture)
		{
		if ( getTexture() != &texture )
			{
			sf::Sprite::setTexture(texture);
			Game::GetGame()->GetCurrentScene()->SetResortSprites();
			}
		}
	}