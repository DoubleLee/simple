#pragma once
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>

#include <string>
#include <memory>

#include "Logger.hpp"

namespace tinyxml2
{
class XMLElement;
}

using namespace std;

namespace Simple
{
class Scene;
class GameObject;
class Component;

class Game : public sf::RenderWindow
	{
	public:
		Game(sf::VideoMode mode, const string & windowTitle);
		virtual ~Game();

		int Run();

		void LoadGameFile(const string & filePath);
		void LoadScene(const string & filePath);

		virtual void OnSceneLoaded();

		static Game * GetGame();
		static const sf::Time & GetTimeSinceGameLoad();
		static const sf::Time & GetDeltaTime();
		static const sf::Time & GetPhysicsTimeStep();
		Scene * GetCurrentScene();

		unique_ptr<Component> LoadEngineComponent(GameObject * pParent, tinyxml2::XMLElement * pComponent);
		Logger & GetLogger();
	private:
		Logger logger;
		static Game * pGame;
		vector<string> sceneFiles;
		string currentSceneFile;
		std::unique_ptr<Scene> pCurrentScene;
		sf::Clock timer;
		sf::Time lastFrame;
		sf::Time delta;
		sf::Font basicFont;
		sf::Text fpsText;
		sf::Time lastFPSDraw;

		void SetGameSingleton();
		virtual void Update();
		virtual void Draw();

		virtual unique_ptr<Component> LoadGameComponent(GameObject * pParent, tinyxml2::XMLElement * pComponent, const string & typeString) = 0;
	};

}