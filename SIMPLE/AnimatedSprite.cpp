#include "AnimatedSprite.hpp"

#include "tinyxml2.hpp"
#include "Texture.hpp"
#include "Game.hpp"
#include "Scene.hpp"
#include <sstream>

namespace Simple
{
	
	void AnimatedSprite::Constructor()
		{
		Sprite::Constructor();
		}

	void AnimatedSprite::Initialize()
		{
		
		}

	void AnimatedSprite::UpdateEarly()
		{

		}

	void AnimatedSprite::Update()
		{
		UpdateAnimation();
		}

	void AnimatedSprite::UpdateLate()
		{

		}

	void AnimatedSprite::UpdatePhysics()
		{

		}

	void AnimatedSprite::OnCollisionEnter(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
		{

		}

	void AnimatedSprite::OnCollisionExit(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
		{

		}

	void AnimatedSprite::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
		{
		Sprite::SaveToXML(pComponentEle, doc);

		if ( animType == AnimationType::Sheet )
			{
			pComponentEle->SetAttribute("FramesX", spriteSheetFramesX);
			pComponentEle->SetAttribute("FramesY", spriteSheetFramesY);
			}
		auto fps = (sf::milliseconds(1000) / timePerFrame);
		pComponentEle->SetAttribute("FPS", fps);
		}

	void AnimatedSprite::LoadFromXML(tinyxml2::XMLElement* pComponent)
		{
		string animTypeString = pComponent->Attribute("AnimType");

		sf::Vector2f size;
		pComponent->QueryFloatAttribute("SizeX", &size.x);
		pComponent->QueryFloatAttribute("SizeY", &size.y);

		float framesPerSecond = 30.0f;
		pComponent->QueryFloatAttribute("FPS", &framesPerSecond);
		timePerFrame = sf::milliseconds(1000) / framesPerSecond;

		pComponent->QueryIntAttribute("Layer", &layer);
	
		sf::Vector2f pos;
		pComponent->QueryFloatAttribute("PosX", &pos.x);
		pComponent->QueryFloatAttribute("PosY", &pos.y);

		float angle = 0;
		pComponent->QueryFloatAttribute("Angle", &angle);

		if ( animTypeString == "SpriteSheet" )
			{
			animType = AnimationType::Sheet;
			int texIndex = -1;
			pComponent->QueryIntAttribute("TexIndex", &texIndex);
			if ( texIndex > -1 )
				{
				setTexture(Game::GetGame()->GetCurrentScene()->GetTextureBy(texIndex));
				}
			else
				{
				string filePath = pComponent->Attribute("TexFilePath");
				setTexture(Game::GetGame()->GetCurrentScene()->GetTextureBy(filePath));
				}
			spriteSheetFramesX = pComponent->IntAttribute("FramesX");
			spriteSheetFramesY = pComponent->IntAttribute("FramesY");
			auto texBounds = getTextureRect();
			spriteSheetPixelsPerFrameX = texBounds.width / spriteSheetFramesX;
			spriteSheetPixelsPerFrameY = texBounds.height / spriteSheetFramesY;

			setScale(size.x / float(spriteSheetPixelsPerFrameX), size.y / float(spriteSheetPixelsPerFrameY));
			setOrigin((float(spriteSheetPixelsPerFrameX) / 2.0f), (float(spriteSheetPixelsPerFrameY) / 2.0f));
			}
		else if ( animTypeString == "TextureArray" )
			{
			animType = AnimationType::Array;
			
			stringstream animationIndices;
			animationIndices << pComponent->Attribute("TexIndicies");
			Scene * pScene = Game::GetGame()->GetCurrentScene();
			int result = -1;
			while(animationIndices)
				{
				animationIndices >> result;
				textureArray.push_back(&pScene->GetTextureBy(result));
				}

			setTexture(*textureArray.at(0));

			auto texRect = getTextureRect();

			setScale(size.x / float(texRect.width), size.y / float(texRect.height));
			setOrigin((float(texRect.width) / 2.0f), (float(texRect.height) / 2.0f));
			}
		else
			throw invalid_argument("Animation type not supported, " + animTypeString);

		setPosition(pos);
		setRotation(angle);
		}

AnimatedSprite::AnimatedSprite()
	:
	Sprite(),
	animType(AnimationType::Null),
	spriteSheetFramesX(0),
	spriteSheetFramesY(0),
	spriteSheetPixelsPerFrameX(0),
	spriteSheetPixelsPerFrameY(0),
	timePerFrame(sf::milliseconds(0)),
	nextFrameTrigger(sf::milliseconds(0)),
	currentFrame(0)
	{

	}


AnimatedSprite::~AnimatedSprite()
	{
	}

void AnimatedSprite::UpdateAnimation()
	{
	Game * pGame = Game::GetGame();
	if ( nextFrameTrigger <= pGame->GetTimeSinceGameLoad() )
		{
		currentFrame = currentFrame % ((animType == AnimationType::Sheet) ? (spriteSheetFramesX * spriteSheetFramesY) : textureArray.size());
		
		if ( animType == AnimationType::Sheet )
			{
			sf::IntRect textRect;

			textRect.left = (currentFrame % spriteSheetFramesX) * spriteSheetPixelsPerFrameX;
			textRect.top = (currentFrame / spriteSheetFramesX)  * spriteSheetPixelsPerFrameY;
			textRect.width = spriteSheetPixelsPerFrameX;
			textRect.height = spriteSheetPixelsPerFrameY;
			setTextureRect(textRect);
			}
		else if ( animType == AnimationType::Array )
			{
			setTexture( *textureArray.at(currentFrame) );
			}
		
		nextFrameTrigger = pGame->GetTimeSinceGameLoad() + timePerFrame;
		++currentFrame;
		}
	
	}

	sf::Time AnimatedSprite::GetAnimationTimeLength() const
		{
		return timePerFrame * ((animType == AnimationType::Sheet) ? sf::Int64(spriteSheetFramesX * spriteSheetFramesY) : sf::Int64(textureArray.size()));
		}

	AnimationType AnimatedSprite::GetAnimationType() const
	{
	return animType;
	}
	}