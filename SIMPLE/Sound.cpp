#include "Sound.hpp"

#include "Game.hpp"
#include "Scene.hpp"
#include "GameObject.hpp"
#include "Sprite.hpp"
#include "tinyxml2.hpp"

#include "SoundBuffer.hpp"

namespace Simple
{

Sound::Sound()
	:
	sf::Sound(),
	Component(),
	autoAttachSprite(true)
	{

	}


Sound::~Sound()
	{
	stop();
	}

void Sound::Constructor()
	{
	if ( autoAttachSprite )
		gameObjectSprite = GetParent()->GetComponent<Sprite>();
	}

void Sound::UpdateLate()
	{
	if ( autoAttachSprite )
		{
		auto spritePos = gameObjectSprite.lock()->getPosition();
		setPosition(spritePos.x, spritePos.y, 0);
		}
	}

void Sound::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("AutoAttachSprite", autoAttachSprite);
	pComponentEle->SetAttribute("DefaultSoundIndex", Game::GetGame()->GetCurrentScene()->GetSoundIndexBy(getBuffer()));
	pComponentEle->SetAttribute("Volume", getVolume());
	}

void Sound::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	pComponent->QueryBoolAttribute("AutoAttachSprite", &autoAttachSprite);
	
	int defaultSoundIndex = -1;

	pComponent->QueryIntAttribute("DefaultSoundIndex", &defaultSoundIndex);

	if ( defaultSoundIndex >= 0 )
		{
		setBuffer( Game::GetGame()->GetCurrentScene()->GetSoundBufferBy(defaultSoundIndex));
		}

	float volume = 100;

	pComponent->QueryFloatAttribute("Volume", &volume);

	setVolume(volume);
	}
}