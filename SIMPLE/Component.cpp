#include "Component.hpp"

#include "Utilities.hpp"

namespace Simple
{

Component::Component()
	:
	pParent(nullptr)
	{
	
	}


Component::~Component()
	{

	}

void Component::Constructor()
	{

	}

void Component::Initialize()
	{
	}

	void Component::UpdateEarly()
		{

		}

void Component::Update()
	{

	}

void Component::UpdateLate()
	{

	}

void Component::OnCollisionEnter(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
	{
	}

	void Component::OnCollisionStay(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
		{
		}

	void Component::OnCollisionExit(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider)
		{
		}

	GameObject* Component::GetParent() const
	{
	return pParent;
	}

	void Component::SetParent(GameObject* pParent)
		{
		if ( (this->pParent = pParent) == nullptr )
			throw std::invalid_argument("A Component's parent was set to nullptr");
		}

	std::string Component::GetName() const
	{
	return std::move(Utilities::GetClassName(typeid(*this)));
	}

void Component::UpdatePhysics()
	{
	}
	}