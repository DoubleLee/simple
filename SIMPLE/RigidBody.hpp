#pragma once
#include "Component.hpp"
#include <memory>
#include <Box2D/Dynamics/b2Body.h>

namespace Simple
{
class Collider;
class Sprite;

class RigidBody : public Component
	{
public:
	void UpdatePhysics() override;
	explicit RigidBody();
	~RigidBody();

	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponent) override;

	void Constructor() override;
	void Initialize() override;

	void UpdateSprite();
	b2Body * GetBody() const;
private:
		b2Body * pBody;// the rigidbody in the physics world.
		weak_ptr<Collider> wpCollider;
		weak_ptr<Sprite> wpSprite;
		unique_ptr<b2BodyDef> pBodyDef;
	};
}
