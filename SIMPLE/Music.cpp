#include "Music.hpp"

#include "tinyxml2.hpp"
namespace Simple
{

void Music::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("FilePath", musicFile.c_str());
	pComponentEle->SetAttribute("AutoPlay", autoPlay);
	pComponentEle->SetAttribute("Loop", loop);
	pComponentEle->SetAttribute("Volume", getVolume());
	}

void Music::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	musicFile = pComponent->Attribute("FilePath");
	pComponent->QueryBoolAttribute("AutoPlay", &autoPlay);
	pComponent->QueryBoolAttribute("Loop", &loop);
	
	float volume = 1.0f;
	pComponent->QueryFloatAttribute("Volume", &volume);
	setVolume(volume);

	if (!this->openFromFile(musicFile))
		throw invalid_argument("Music file was not found or corrupt, " + musicFile);
	}

Music::Music()
	:
	sf::Music(),
	Component(),
	autoPlay(false),
	loop(false)
	{
	
	}

Music::~Music()
	{

	}

void Music::Constructor()
	{
	if ( autoPlay )
		play();

	setLoop(loop);
	}
}