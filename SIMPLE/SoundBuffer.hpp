#pragma once
#include <SFML/Audio/SoundBuffer.hpp>

namespace Simple
{
class SoundBuffer : public sf::SoundBuffer
	{
	public:
		SoundBuffer();
		~SoundBuffer();

		void LoadFromFile(const std::string & filePath);

		const std::string & GetFilePath() const;

	protected:
		std::string filePath;
	};

}