#pragma once
#include <SFML/Graphics/Texture.hpp>

namespace Simple
{

class Texture : public sf::Texture
{
public:
	Texture();
	~Texture();

	void LoadFromFile(const std::string & filePath);

	const std::string & GetFilePath() const;

protected:
	std::string filePath;
};

}