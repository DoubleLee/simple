#pragma once

#include <string>
#include <vector>
#include <memory>

#include <SFML/System/Time.hpp>
#include <Box2D/Dynamics/b2WorldCallbacks.h>

namespace tinyxml2{
	class XMLElement;
	}

namespace sf{
	class SoundBuffer;
	class Texture;
	}

using namespace std;

class b2World;

namespace Simple
{
class GameObject;
class Texture;
class SoundBuffer;
class Sprite;
class Prefab;

class Scene : public b2ContactListener
	{
	friend class Prefab;
public:
		explicit Scene();
		~Scene();

		const string & GetFilePath() const;

		void Construct();
		void Initialize();
		void Update();
		void Draw();
		

		Texture & GetTextureBy(int index);
		Texture & GetTextureBy(const string & filePath);
		int GetTextureIndexBy(const sf::Texture * texture);

		SoundBuffer & GetSoundBufferBy(int index);
		const SoundBuffer & GetSoundBufferBy(const string & filePath);
		int GetSoundIndexBy(const sf::SoundBuffer * soundBuffer);

		void LoadTexture(const string & filePath);
		void LoadSoundBuffer(const string & filePath);
		void LoadScene(const string & filePath);
		void SaveScene(const string & filePath);

		weak_ptr<GameObject> GetGameObjectBy(const string & tag);
		vector<weak_ptr<GameObject>> GetGameObjectsBy(const string & tag);
		b2World * GetWorld() const;

		void BeginContact(b2Contact* pContact) override;
		void StayContact(b2Contact * pContact);
		void EndContact(b2Contact* pContact) override;
		void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
		void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override;
		const sf::Time & GetPhysicsTimeStep() const;
		void AddSprite(Sprite * pSprite);
		void RemoveSprite(Sprite * pSprite);
		shared_ptr<GameObject> InstantiatePrefab(const string & filePath);
		void SetResortSprites();
	private:
		shared_ptr<GameObject> LoadGameObject(tinyxml2::XMLElement * pGameObject, bool directIntoMain = false);
		void EraseTaggedGameObjects();
		void CombineAddedGameObjects();
		void UpdatePhysics();
		void UpdateCollisionStay();
		bool resortSprites;
		string filePath;
		unique_ptr<b2World> pWorld;
		vector<unique_ptr<Texture>> textures;
		vector<unique_ptr<SoundBuffer>> soundBuffers;
		// sprite cache
		// sprites add and remove themselves from this automatically.
		// AddSprite will sort this automatically.
		std::vector<unique_ptr<Prefab>> prefabs;
		vector<Sprite*> sceneSprites;
		vector<shared_ptr<GameObject>> gameObjects;
		vector<shared_ptr<GameObject>> addedGameObjects;
		sf::Time physicsTimeStep;
		sf::Time lastPhysicsUpdate;
	};

}