#include "RectangleCollider.hpp"

#include "GameObject.hpp"
#include "RigidBody.hpp"
#include "Sprite.hpp"
#include "Math.hpp"
#include "tinyxml2.hpp"

#include <Box2D/Collision/Shapes/b2PolygonShape.h>

namespace Simple
{
	RectangleCollider::RectangleCollider()
		:
		Collider(),
		pFixtureDef(make_unique<b2FixtureDef>())
		{
		}
	
	RectangleCollider::~RectangleCollider()
		{
		}

	void RectangleCollider::Constructor()
		{
		Collider::Constructor(); // gets some refs for us.
		}

	void RectangleCollider::Initialize()
		{
		const auto & spSprite = wpSprite.lock();
		auto bounds = spSprite->getTextureRect();
		auto scales = spSprite->getScale();
		float width = float(bounds.width) * scales.x;
		float height = float(bounds.height) *  scales.y;

		b2FixtureDef & fixtureDef = *pFixtureDef;
		fixtureDef.userData = this;

		b2PolygonShape shape;
		shape.SetAsBox(Math::PixelsToMeters(width / 2.0f), Math::PixelsToMeters(height / 2.0f));
		fixtureDef.shape = &shape;

		SetFixture(wpBody.lock()->GetBody()->CreateFixture(&fixtureDef));
		pFixtureDef.release();
		}

	void RectangleCollider::UpdateEarly()
		{
		}

	void RectangleCollider::Update()
		{
		}

	void RectangleCollider::UpdateLate()
		{
		}

void RectangleCollider::SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc)
	{
	b2Fixture * pFixture = GetFixture();
	
	pComponentEle->SetAttribute("Friction", pFixture->GetFriction());
	pComponentEle->SetAttribute("Density", pFixture->GetDensity());
	pComponentEle->SetAttribute("Restitution", pFixture->GetRestitution());
	}

void RectangleCollider::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	b2FixtureDef & fixtureDef = *pFixtureDef;
	pComponent->QueryFloatAttribute("Friction", &fixtureDef.friction);
	pComponent->QueryFloatAttribute("Density", &fixtureDef.density);
	pComponent->QueryFloatAttribute("Restitution", &fixtureDef.restitution);
	}

}