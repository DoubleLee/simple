#pragma once

#include <SFML/Graphics/Sprite.hpp>

#include <vector>
#include <memory>

class b2Contact;

namespace tinyxml2
{
	class XMLDocument;
	class XMLElement;
}

using namespace std;

namespace Simple
{
	class Collider;
	class Component;

class GameObject
	{
	public:
		explicit GameObject();
		~GameObject();

		void SaveToXML(tinyxml2::XMLElement * pGameObjectsEle, tinyxml2::XMLDocument & doc );
		void LoadFromXML(tinyxml2::XMLElement * pGameObject);

		void Constructor();
		void Initialize();

		void UpdateEarly();
		void Update();
		void UpdateLate();

		void UpdatePhysics();

		void OnCollisionEnter(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		void OnCollisionStay(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		void OnCollisionExit(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		
		template<class T> weak_ptr<T> GetComponent();
		template<class T> vector<weak_ptr<T>> GetComponents();

		const string & GetTag() const;
		void SetTag(const string & tag);

		bool TagEquals(const string & otherTag) const;
		bool GetTaggedForDelete() const;
		void SetTaggedForDelete(bool deleteThis = true);
		bool isConstructed() const;
		bool isInitialized() const;

		void SetConstructed();
		void SetInitialized();
	private:
		string tag;
		std::vector<shared_ptr<Component>> components;
		bool taggedForDelete;
		bool constructed;
		bool initialized;
		
	};

template <class T> weak_ptr<T> GameObject::GetComponent()
	{
	for ( size_t i = 0; i < components.size(); ++i )
		{
		auto & spComponent = components[i];

		const auto & pType = std::dynamic_pointer_cast<T>(spComponent);

		if ( pType != nullptr )
			return pType;
		}

	return weak_ptr<T>();
	}

template <class T> vector<weak_ptr<T>> GameObject::GetComponents()
	{
	vector<weak_ptr<T>> result;

	for ( size_t i = 0; i < components.size(); ++i )
		{
		auto & spComponent = components[i];

		const auto & pType = std::dynamic_pointer_cast<T>(spComponent);

		if ( pType != nullptr )
			{
			result.emplace_back(pType);
			}
		}

	return std::move(result);
	}
}
