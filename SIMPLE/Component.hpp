#pragma once
#include <string>
#include <memory> // for sub classes

class b2Contact;

namespace tinyxml2
{
class XMLDocument;
class XMLElement;
}

using namespace std;

namespace Simple
{
	class Game;
	class Scene;
	class GameObject;
	class Sprite;
	class Music;
	class Sound;
	class Texture;
	class SoundBuffer;
	class AnimatedSprite;
	class RigidBody;
	class Collider;

	class Component
	{
	public:
		explicit Component();
		virtual ~Component();

		virtual void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) = 0;
		virtual void LoadFromXML(tinyxml2::XMLElement * pComponentEle) = 0;
		
		/// Use only for grabbing references to other GameObjects or Components.
		virtual void Constructor();

		/// Use for actually running code on various components, which are guaranteed to exist
		/// and be constructed and initialize.
		virtual void Initialize();

		virtual void UpdateEarly();

		virtual void Update();

		virtual void UpdateLate();

		virtual void UpdatePhysics();

		virtual void OnCollisionEnter(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		virtual void OnCollisionStay(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		virtual void OnCollisionExit(b2Contact * pContact, Collider * pThisCollider, Collider * pOtherCollider);
		GameObject * GetParent() const;
		void SetParent(GameObject * pParent);

		string GetName() const;
		
	private:
		GameObject * pParent;
	};

}