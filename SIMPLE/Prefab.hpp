#pragma once
#include <string>

#include "tinyxml2.hpp"

namespace Simple
{
class GameObject;

class Prefab
{
public:
	explicit Prefab(const std::string & filePath);
	~Prefab();

	const std::string & GetFilePath() const;

	tinyxml2::XMLElement * GetGameObjectElement();

private:
	Prefab(const Prefab & other) = delete;
	tinyxml2::XMLDocument prefabDoc;
	tinyxml2::XMLElement * pGameObject;
	std::string filePath;
};

}