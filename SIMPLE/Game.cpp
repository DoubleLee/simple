#include "Game.hpp"

#include "../SIMPLE/Scene.hpp"
#include "../SIMPLE/GameObject.hpp"
#include "tinyxml2.hpp"

#include <stdexcept>
#include <fstream>
#include <string>
#include "Sprite.hpp"
#include "Music.hpp"
#include "Sound.hpp"
#include "RigidBody.hpp"
#include "RectangleCollider.hpp"
#include "CircleCollider.hpp"
#include "Math.hpp"
#include "AnimatedSprite.hpp"


namespace Simple
{

Game * Game::pGame = nullptr;

Game * Game::GetGame()
	{
	if ( pGame != nullptr )
		{
		return pGame;
		}
	else
		{
		throw runtime_error("Game singleton was null when something had requested it.");
		}
	}

Game::Game(sf::VideoMode mode, const std::string& windowTitle)
	:
	sf::RenderWindow(mode, windowTitle)
	{
	SetGameSingleton();
	basicFont.loadFromFile("arial.ttf");
	fpsText.setFont(basicFont);
	lastFPSDraw = sf::milliseconds(-1100);
	this->setVerticalSyncEnabled(false);
	Math::SetSeedByTime();
	
	}

Game::~Game()
	{
	pCurrentScene.release(); // must happen so pGame is valid on destruction.
	pGame = nullptr;
	}

int Game::Run()
	{
	timer.restart();
	lastFrame = timer.getElapsedTime();
	while(this->isOpen())
		{
		sf::Event event;
		while(this->pollEvent(event))
			{
			if(event.type == sf::Event::Closed)
				this->close();
			// TODO: write some input for button ondown event, and onup events
			// default sfml only tells you if the current state is down or up
			// I'd like to add my own system to include down up event checks
			// which boil the idea down to a simple bool getter for those events
			// on a frame bases.
			}

		this->Update();
		this->clear();
		this->Draw();
		this->display();
		}

	return 0;
	}

void Game::LoadGameFile(const string & filePath)
	{
	// simple file 
	// line 1 is index of default scene, 
	// line 2 is number of scenes in the file
	// subsequent lines are the array of scene files

	ifstream gameFile(filePath);

	if ( gameFile.good() )
		{
		int defaultSceneIndex;
		gameFile >> defaultSceneIndex;

		int sceneCount;
		gameFile >> sceneCount;

		sceneFiles.clear();
		sceneFiles.reserve(sceneCount);

		ifstream sceneFile;
		string buffer;
		for ( int i = 0; i < sceneCount && gameFile.good(); ++i )
			{
			gameFile >> buffer;

			sceneFile.open(buffer);

			if ( sceneFile.good() )
				{
				sceneFile.close();
				sceneFiles.emplace_back(std::move(buffer));
				}
			else
				{
				throw runtime_error("Within game file, a scene file entry was not found.");
				}
			}

		LoadScene(sceneFiles[defaultSceneIndex]);
		}
	}

void Game::LoadScene(const string& filePath)
	{
	pCurrentScene.release();
	pCurrentScene = std::make_unique<Scene>();
	pCurrentScene->LoadScene(filePath);
	pCurrentScene->Construct();
	pCurrentScene->Initialize();
	OnSceneLoaded();
	}

void Game::OnSceneLoaded()
	{
	}

	const sf::Time & Game::GetTimeSinceGameLoad()
	{
	return GetGame()->lastFrame;
	}

const sf::Time & Game::GetDeltaTime()
	{
	return GetGame()->delta;
	}

const sf::Time& Game::GetPhysicsTimeStep()
	{
	return GetGame()->GetCurrentScene()->GetPhysicsTimeStep();
	}

	Scene* Game::GetCurrentScene()
	{
	return pCurrentScene.get();
	}

void Game::SetGameSingleton()
	{
	if ( pGame == nullptr )
		{
		pGame = this;
		}
	else
		{
		throw runtime_error("Game singleton already set when attempting to set game singleton again.");
		}
	}

void Game::Update()
	{
	sf::Time thisFrame = timer.getElapsedTime();

	delta = thisFrame - lastFrame;

	lastFrame = thisFrame;
	static int frameCount = 0;
	++frameCount;
	sf::Time sinceLastFPSDraw( thisFrame - lastFPSDraw);
	if ( sinceLastFPSDraw >= sf::milliseconds(1000) )
		{
		fpsText.setString("FPS: " + to_string(int(float(frameCount) / sinceLastFPSDraw.asSeconds())));
		frameCount = 0;
		lastFPSDraw = thisFrame;
		}

	if ( pCurrentScene )
		{
		pCurrentScene->Update();
		}
	else
		{
		throw std::runtime_error("Game has attempted to update a null scene.");
		}
	}

void Game::Draw()
	{
	if ( pCurrentScene )
		{
		pCurrentScene->Draw();

		this->draw(fpsText);
		}
	else
		{
		throw std::runtime_error("Game has attempted to draw a null scene.");
		}
	}

unique_ptr<Component> Game::LoadEngineComponent(GameObject* pParent, tinyxml2::XMLElement* pComponent)
	{
	unique_ptr<Component> pComp;

	string typeString = pComponent->Name();

	if (typeString == "Sprite")
		{
		pComp = make_unique<Sprite>();
		}
	else if (typeString == "Music")
		{
		pComp = make_unique<Music>();
		}
	else if (typeString == "Sound")
		{
		pComp = make_unique<Sound>();
		}
	else if (typeString == "RigidBody")
		{
		pComp = make_unique<RigidBody>();
		}
	else if (typeString == "RectangleCollider")
		{
		pComp = make_unique<RectangleCollider>();
		}
	else if (typeString == "CircleCollider")
		{
		pComp = make_unique<CircleCollider>();
		}
	else if (typeString == "AnimatedSprite")
		{
		pComp = make_unique<AnimatedSprite>();
		}

	if (pComp || (pComp = LoadGameComponent(pParent, pComponent, typeString)) )
		{
		pComp->SetParent(pParent);
		pComp->LoadFromXML(pComponent);
		return pComp;
		}
	else
		{
		throw invalid_argument("The game class, and the child of the game  class returned null for the requested component, " + typeString);
		}
	}

	Logger& Game::GetLogger()
		{
		return logger;
		}
	}