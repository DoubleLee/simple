#include "CoolDown.hpp"

namespace Simple
{

CoolDown::CoolDown(sf::Time coolDownAmount)
	:
	coolDownAmount(coolDownAmount),
	nextCoolDownTrigger()
	{
	}


CoolDown::~CoolDown()
	{
	}

bool CoolDown::HasCooledDown(sf::Time timeSinceLoad)
	{
	if ( timeSinceLoad >= nextCoolDownTrigger )
		{
		nextCoolDownTrigger = coolDownAmount + timeSinceLoad;
		return true;
		}
	else
		return false;
	}

}