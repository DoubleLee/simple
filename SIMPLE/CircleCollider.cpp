#include "CircleCollider.hpp"

#include "GameObject.hpp"
#include "RigidBody.hpp"
#include "Sprite.hpp"
#include "Math.hpp"
#include "tinyxml2.hpp"

#include <Box2D/Collision/Shapes/b2CircleShape.h>

namespace Simple
{
	CircleCollider::CircleCollider()
		:
		Collider(),
		pFixtureDef(make_unique<b2FixtureDef>())
		{
		}
	
	CircleCollider::~CircleCollider()
		{
		}

	void CircleCollider::Constructor()
		{
		Collider::Constructor(); // gets some refs for us.
		}

	void CircleCollider::Initialize()
		{
		const auto & spSprite = wpSprite.lock();
		auto bounds = spSprite->getTextureRect();
		auto scales = spSprite->getScale();
		float width = float(bounds.width) * scales.x;

		b2FixtureDef & fixtureDef = *pFixtureDef;
		fixtureDef.userData = this;

		b2CircleShape circleShape;
		circleShape.m_radius = Math::PixelsToMeters(width / 2.0f);
		fixtureDef.shape = &circleShape;

		SetFixture(wpBody.lock()->GetBody()->CreateFixture(&fixtureDef));
		pFixtureDef.release();
		}

void CircleCollider::UpdateEarly()
	{
	}

void CircleCollider::Update()
	{
	}

void CircleCollider::UpdateLate()
	{
	}

void CircleCollider::SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc)
	{
	b2Fixture * pFixture = GetFixture();
	
	pComponentEle->SetAttribute("Friction", pFixture->GetFriction());
	pComponentEle->SetAttribute("Density", pFixture->GetDensity());
	pComponentEle->SetAttribute("Restitution", pFixture->GetRestitution());
	}

void CircleCollider::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	b2FixtureDef & fixtureDef = *pFixtureDef;
	pComponent->QueryFloatAttribute("Friction", &fixtureDef.friction);
	pComponent->QueryFloatAttribute("Density", &fixtureDef.density);
	pComponent->QueryFloatAttribute("Restitution", &fixtureDef.restitution);
	}


}