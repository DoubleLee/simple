#include "RigidBody.hpp"
#include "Game.hpp"
#include "Scene.hpp"
#include "Collider.hpp"
#include "GameObject.hpp"

#include "Sprite.hpp"
#include "Math.hpp"

#include "tinyxml2.hpp"

#include <Box2D/Dynamics/b2World.h>

#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

namespace Simple
{
void RigidBody::UpdateSprite()
	{
	const auto & pSprite = wpSprite.lock();

	const auto & bodyPos = pBody->GetPosition();
	pSprite->setPosition( Math::MetersToPixels(bodyPos.x), Math::MetersToPixels(bodyPos.y));
	pSprite->setRotation( pBody->GetAngle() * RADTODEG);
	}
void RigidBody::UpdatePhysics()
	{
	
	}

RigidBody::RigidBody()
	:
	Component(),
	pBody(nullptr),
	pBodyDef(make_unique<b2BodyDef>())
	{
	
	}

RigidBody::~RigidBody()
	{
	}

void RigidBody::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	b2Body * pBody = GetBody();

	string typeString;

	switch(pBody->GetType())
		{
		case b2_staticBody:
			{
			typeString = "static";
			break;
			}
		case b2_kinematicBody:
			{
			typeString = "kinematic";
			break;
			}
		case b2_dynamicBody:
			{
			typeString=  "dynamic";
			break;
			}
		default:
			{
			throw invalid_argument("The type of physics body didn't match any known, " + to_string(pBody->GetType()));
			}
		}
	pComponentEle->SetAttribute("Type", typeString.c_str());
	pComponentEle->SetAttribute("FixedRotation", pBody->IsFixedRotation());
	pComponentEle->SetAttribute("IsBullet", pBody->IsBullet());
	pComponentEle->SetAttribute("LinearDamping", pBody->GetLinearDamping());
	pComponentEle->SetAttribute("AngularDamping", pBody->GetAngularDamping());
	}

void RigidBody::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	auto & body = *pBodyDef.get();
	
	string typeString = pComponentEle->Attribute("Type");

	if ( typeString == "static" )
		{
		body.type = b2_staticBody;
		}
	else if ( typeString == "kinematic" )
		{
		body.type = b2_kinematicBody;
		}
	else if ( typeString == "dynamic" )
		{
		body.type = b2_dynamicBody;
		}
	else
		{
		throw invalid_argument("RigidBody had unknown physics body type, " + typeString);
		}

	pComponentEle->QueryBoolAttribute("FixedRotation", &body.fixedRotation);
	pComponentEle->QueryBoolAttribute("IsBullet", &body.bullet);
	pComponentEle->QueryFloatAttribute("LinearDamping", &body.linearDamping);
	pComponentEle->QueryFloatAttribute("AngularDamping", &body.angularDamping);
	pComponentEle->QueryFloatAttribute("LinearVelocityX", &body.linearVelocity.x);
	pComponentEle->QueryFloatAttribute("LinearVelocityY", &body.linearVelocity.y);
	}

void RigidBody::Constructor()
	{
	wpCollider = GetParent()->GetComponent<Collider>();
	wpSprite = GetParent()->GetComponent<Sprite>();

	auto pWorld = Game::GetGame()->GetCurrentScene()->GetWorld();
	pBodyDef->userData = this;
	pBody = pWorld->CreateBody(pBodyDef.get());
	pBodyDef.release();
	}

void RigidBody::Initialize()
	{
	const auto & spSprite = wpSprite.lock();
	//const auto & spCollider = wpCollider.lock();

	const auto & bodyPos = spSprite->getPosition();
	
	GetBody()->SetTransform(b2Vec2(Math::PixelsToMeters(bodyPos.x), Math::PixelsToMeters(bodyPos.y)), spSprite->getRotation() * DEGTORAD);
	}

	b2Body* RigidBody::GetBody() const
		{
		if ( pBody )
			return pBody;
		else
			throw runtime_error("Rigidbody has no b2Body but it was requested.");
		}
	}