#pragma once
#include "Collider.hpp"

namespace Simple
{
class CircleCollider : public Collider
{
public:
	
	explicit CircleCollider();
	~CircleCollider();

	void Constructor() override;
	void Initialize() override;

	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;	
	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponent) override;


private:
	unique_ptr<b2FixtureDef> pFixtureDef;
	
};

}