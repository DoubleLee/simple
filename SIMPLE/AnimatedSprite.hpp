#pragma once
#include "Sprite.hpp"
#include <SFML/System/Time.hpp>
#include <vector>

namespace Simple
{
enum class AnimationType
	{
	Null,
	Sheet, // A single texture with seperate frames layed out as a sheet.
	Array, // Array of seperate textures
	};
class AnimatedSprite : public Simple::Sprite
{
public:
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;
	void UpdatePhysics() override;
	void OnCollisionEnter(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider) override;
	void OnCollisionExit(b2Contact* pContact, Collider* pThisCollider, Collider* pOtherCollider) override;
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponent) override;
	explicit AnimatedSprite();
	~AnimatedSprite();

	void UpdateAnimation();

	sf::Time GetAnimationTimeLength() const;

	AnimationType GetAnimationType() const;
private:
	AnimationType animType;
	int spriteSheetFramesX;
	int spriteSheetFramesY;
	int spriteSheetPixelsPerFrameX;
	int spriteSheetPixelsPerFrameY;
	sf::Time timePerFrame;
	sf::Time nextFrameTrigger;
	int currentFrame;
	vector<Texture*> textureArray;
};

}