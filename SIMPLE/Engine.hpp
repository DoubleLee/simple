#pragma once

#include "Game.hpp"
#include "Scene.hpp"
#include "GameObject.hpp"
#include "AnimatedSprite.hpp" // also includes Sprite.hpp
#include "RigidBody.hpp"
#include "Collider.hpp"
#include "Texture.hpp"
#include "SoundBuffer.hpp"
#include "Sound.hpp"
#include "Music.hpp"
#include "Logger.hpp"

#include "Math.hpp"
#include "Utilities.hpp"
#include "CoolDown.hpp"

#include "tinyxml2.hpp"