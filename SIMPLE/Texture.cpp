#include "Texture.hpp"

namespace Simple
{

Texture::Texture()
	:
	sf::Texture()
	{
	}


Texture::~Texture()
	{
	}

void Texture::LoadFromFile(const std::string& filePath)
	{
	if (loadFromFile(filePath))
		{
		this->filePath = filePath;
		}
	else
		throw std::invalid_argument("Texture file not found or unreadable, " + filePath);
	}

const std::string& Texture::GetFilePath() const
	{
	return filePath;
	}
}