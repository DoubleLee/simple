#pragma once

#include <memory>

#include "Component.hpp"
#include <SFML/Audio/Sound.hpp>

#include <SFML/System/Vector2.hpp>


namespace Simple
{
class Sprite;

class Sound : public sf::Sound, public Component
{
public:
	explicit Sound();
	~Sound();

	void Constructor() override;
	void UpdateLate() override;

	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponent) override;

private:
	bool autoAttachSprite;
	weak_ptr<Sprite> gameObjectSprite;
	sf::Vector2f lastPosition;
};

}