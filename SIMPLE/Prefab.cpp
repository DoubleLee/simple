#include "Prefab.hpp"

#include <string>

namespace Simple
{
Prefab::Prefab(const std::string & filePath)
	:
	prefabDoc()
	{
	auto result = prefabDoc.LoadFile(filePath.c_str());

	if ( result == tinyxml2::XML_SUCCESS && (pGameObject = prefabDoc.RootElement()->FirstChildElement("GameObject")) )
		{
		this->filePath = filePath;
		}
	else
		{
		throw std::invalid_argument("Failed to load prefab file, " + filePath);
		}
	}

	Prefab::~Prefab()
		{
		}

	const std::string& Prefab::GetFilePath() const
		{
		return filePath;
		}

	tinyxml2::XMLElement * Prefab::GetGameObjectElement()
		{
		return pGameObject;
		}
	}