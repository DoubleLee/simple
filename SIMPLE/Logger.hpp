#pragma once
#include <fstream>

namespace Simple
{
class Logger
{
public:
	Logger();
	~Logger();

	std::ofstream & Log(const std::string & data);

private:
	std::ofstream logStream;

	const char * GetTime();
};

}