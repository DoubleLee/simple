#include "Utilities.hpp"


namespace Simple
{
namespace Utilities
{
std::string GetClassName(const type_info& typeInfo)
	{
	std::string typeName = typeInfo.name();
	
	int result = typeName.find_last_of(':');

	if ( result != std::string::npos )
		{
		return &typeName.at(result + 1);
		}
	else
		{
		result = typeName.find_last_of(' ');
		if ( result != std::string::npos )
			{
			return &typeName.at(result + 1);
			}
		else
			{
			return std::move(typeName);
			}
		}
	}
}
}