#include "Respawner.hpp"

#include "Simple/Sprite.hpp"
#include "Simple/RigidBody.hpp"
#include "Simple/Game.hpp"
#include "Simple/Math.hpp"
#include "Simple/GameObject.hpp"
#include "Simple/tinyxml2.hpp"

#include <Box2D/Dynamics/b2Body.h>


Respawner::Respawner()
	:
	Component()
	{
	}

Respawner::~Respawner()
	{
	
	}

void Respawner::Constructor()
	{
	wpSprite = GetParent()->GetComponent<Simple::Sprite>();
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	}

void Respawner::Initialize()
	{
	}

void Respawner::UpdateEarly()
	{
	}

void Respawner::Update()
	{
	const auto & spSprite = wpSprite.lock();

	const auto & boundingBox = spSprite->getGlobalBounds();
	const auto & windowSize = Simple::Game::GetGame()->getSize();
	if ( boundingBox.top >= windowSize.y)
		{
		const auto & spRigidBody = wpRigidBody.lock();

		spRigidBody->GetBody()->SetTransform(b2Vec2(Simple::Math::PixelsToMeters(Simple::Math::GetRandomBetween(0, windowSize.x)), Simple::Math::PixelsToMeters(Simple::Math::GetRandomBetween(-500, -boundingBox.height))), spRigidBody->GetBody()->GetAngle());
		spRigidBody->GetBody()->SetLinearVelocity(b2Vec2_zero);
		spRigidBody->GetBody()->SetAngularVelocity(0);
		}
	}

void Respawner::UpdateLate()
	{
	}

void Respawner::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	}

void Respawner::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	}
