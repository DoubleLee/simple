#pragma once
#include "Simple/Component.hpp"

#include <memory>
#include <Box2D/Common/b2Math.h>
using namespace std;

namespace Simple
{
class RigidBody;
class Sound;
}

class RocketBounce : public Simple::Component
{
public:
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;
	void OnCollisionEnter(b2Contact * pContact, Simple::Collider * pThisCollider, Simple::Collider * pOtherCollider) override;
	void OnCollisionExit(b2Contact * pContact, Simple::Collider * pThisCollider, Simple::Collider * pOtherCollider) override;
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;

	explicit RocketBounce();
	~RocketBounce();
protected:
	weak_ptr<Simple::RigidBody> wpRigidBody;
	weak_ptr<Simple::Sound> wpSound;
	b2Vec2 force;
};

