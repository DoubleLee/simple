#pragma once
#include "SIMPLE/Component.hpp"
#include <SFML/System/Vector3.hpp>
#include <SFML/System/Vector2.hpp>
#include <memory>

namespace Simple
{
class Sprite;
class Sound;
class RigidBody;
}

namespace Space
{

class PlayerControls : public Simple::Component
{
public:
	explicit PlayerControls();
	~PlayerControls();
	void SaveToXML(tinyxml2::XMLElement * pComponentEle, tinyxml2::XMLDocument & doc) override;
	void LoadFromXML(tinyxml2::XMLElement * pComponent) override;
	void Constructor() override;
	void UpdateEarly() override;
	void UpdatePhysics() override;
	void UpdateLate() override;

protected:
	sf::Vector2f speed;
	weak_ptr<Simple::Sprite> wpSprite;
	weak_ptr<Simple::RigidBody> wpRigidBody;
	weak_ptr<Simple::Sound> wpSound;
};

}