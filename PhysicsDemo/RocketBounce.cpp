#include "RocketBounce.hpp"

#include "Simple/Collider.hpp"
#include "Simple/GameObject.hpp"
#include "Simple/RigidBody.hpp"
#include "Simple/Sound.hpp"

#include "Simple/tinyxml2.hpp"
#include <Simple/Math.hpp>
#include <Simple/Game.hpp>
#include <Simple/Scene.hpp>
#include "Simple/Sprite.hpp"


void RocketBounce::Constructor()
	{
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	wpSound = GetParent()->GetComponent<Simple::Sound>();
	}

void RocketBounce::Initialize()
	{
	}

void RocketBounce::UpdateEarly()
	{
	}

void RocketBounce::Update()
	{
	}

void RocketBounce::UpdateLate()
	{
	}

void RocketBounce::OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	if (pOtherCollider->GetParent()->TagEquals("Platform"))
		{
		const auto & spRigidBody = wpRigidBody.lock();
		const auto & spSound = wpSound.lock();

		b2WorldManifold worldManifold;
		pContact->GetWorldManifold(&worldManifold);

		auto point = worldManifold.points[0];

		b2Body * pBody = spRigidBody->GetBody();
		pBody->ApplyLinearImpulse(force, pBody->GetWorldCenter(), true);

		if ( spSound->getStatus() != sf::SoundSource::Status::Playing )
			spSound->play();
		
		const auto & spExplosion = Simple::Game::GetGame()->GetCurrentScene()->InstantiatePrefab("Explosion.prefab");
		const auto & spExplosionSprite = spExplosion->GetComponent<Simple::Sprite>().lock();
		spExplosionSprite->setPosition(sf::Vector2f(Simple::Math::MetersToPixels(point.x), Simple::Math::MetersToPixels(point.y)));
		spExplosionSprite->setRotation(Simple::Math::GetRandomBetween(0, 361));
		}
	}

void RocketBounce::OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{

	}

void RocketBounce::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("ForceX", force.x);
	pComponentEle->SetAttribute("ForceY", force.y);
	}

void RocketBounce::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	pComponentEle->QueryFloatAttribute("ForceX", &force.x);
	pComponentEle->QueryFloatAttribute("ForceY", &force.y);
	}

RocketBounce::RocketBounce()
	:
	Component()
	{
	}

RocketBounce::~RocketBounce()
{
}
