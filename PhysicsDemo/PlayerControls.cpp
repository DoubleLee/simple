#include "PlayerControls.hpp"

#include "Simple/GameObject.hpp"
#include "Simple/Game.hpp"
#include "Simple/Scene.hpp"
#include "Simple/Sprite.hpp"
#include "Simple/Sound.hpp"
#include "Simple/tinyxml2.hpp"
#include "Simple/RigidBody.hpp"
#include "Simple/Math.hpp"
#include "Simple/SoundBuffer.hpp"

#include <Box2D/Dynamics/b2Body.h>

#include "SFML/Audio/Listener.hpp"

namespace Space
{

PlayerControls::PlayerControls()
	:
	Component()
	{

	}


PlayerControls::~PlayerControls()
	{

	}

void PlayerControls::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("SpeedX", speed.x);
	pComponentEle->SetAttribute("SpeedY", speed.y);
	}

void PlayerControls::LoadFromXML(tinyxml2::XMLElement* pComponent)
	{
	pComponent->QueryFloatAttribute("SpeedX", &speed.x);
	pComponent->QueryFloatAttribute("SpeedY", &speed.y);
	}

void PlayerControls::Constructor()
	{
	wpSprite = GetParent()->GetComponent<Simple::Sprite>();
	wpSound = GetParent()->GetComponent<Simple::Sound>();
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	const auto & spSound = wpSound.lock();
	if ( spSound )
		spSound->setBuffer(Simple::Game::GetGame()->GetCurrentScene()->GetSoundBufferBy("pig.wav"));
	}

void PlayerControls::UpdateEarly()
	{
	}

void PlayerControls::UpdatePhysics()
	{
	sf::Vector2f offset(0,0);

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W) )
		{
		offset.y += -1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) )
		{
		offset.y += 1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A) )
		{
		offset.x += -1;
		}
	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D) )
		{
		offset.x += 1;
		}

	if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift) )
		{
		offset *= 2.0f;
		}

	offset.x *= speed.x;
	offset.y *= speed.y;
	const auto & spRigidBody = wpRigidBody.lock();
	b2Vec2 velocity(Simple::Math::PixelsToMeters(offset.x), Simple::Math::PixelsToMeters(offset.y));

	spRigidBody->GetBody()->SetLinearVelocity(velocity);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S) && sf::Keyboard::isKeyPressed(sf::Keyboard::RControl) )
		{
		Simple::Game::GetGame()->GetCurrentScene()->SaveScene("TestMapOutput.xml");
		}

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
		
		const auto & spSound = wpSound.lock();
		if ( spSound )
			spSound->play();

		}
	}

void PlayerControls::UpdateLate()
	{

	}

}