#include "Box2D_PhysicsDemo.hpp"

#include <fstream>
#include <string>
#include "PlayerControls.hpp"
#include "Respawner.hpp"

#include "Simple/tinyxml2.hpp"
#include "RocketBounce.hpp"
#include "Exploder.hpp"
#include "DeleteAfterExplosion.hpp"

namespace Space
{

Box2D_PhysicsDemo::Box2D_PhysicsDemo()
	:
	Game(sf::VideoMode(800, 600), "Box2D Physics Demo!")
	{

	}


Box2D_PhysicsDemo::~Box2D_PhysicsDemo()
	{

	}

unique_ptr<Component> Box2D_PhysicsDemo::LoadGameComponent(GameObject* pParent, tinyxml2::XMLElement* pComponentEle, const string& typeString)
	{
	unique_ptr<Component> pComponent;

	if (typeString == "PlayerControls")
		{
		pComponent = make_unique<PlayerControls>();
		}
	else if (typeString == "Respawner")
		{
		pComponent = make_unique<Respawner>();
		}
	else if (typeString == "RocketBounce")
		{
		pComponent = make_unique<RocketBounce>();
		}
	else if ( typeString == "Exploder")
		{
		pComponent = make_unique<Exploder>();
		}
	else if ( typeString == "DeleteAfterExplosion" )
		{
		pComponent = make_unique<DeleteAfterExplosion>();
		}

	return pComponent;
	}
}