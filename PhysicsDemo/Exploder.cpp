#include "Exploder.hpp"
#include "Simple/tinyxml2.hpp"
#include "Simple/Collider.hpp"
#include "Simple/RigidBody.hpp"
#include "Simple/GameObject.hpp"
#include "Simple/Sound.hpp"
#include "Simple/Game.hpp"
#include "Simple/Scene.hpp"
#include "Simple/Sprite.hpp"
#include "Simple/Math.hpp"

void Exploder::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	pComponentEle->SetAttribute("Force", force);
	}

void Exploder::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	pComponentEle->QueryFloatAttribute("Force", &force);
	}

void Exploder::Constructor()
	{
	wpRigidBody = GetParent()->GetComponent<Simple::RigidBody>();
	wpSound = GetParent()->GetComponent<Simple::Sound>();
	}

void Exploder::Initialize()
	{
	}

void Exploder::UpdateEarly()
	{
	}

void Exploder::Update()
	{
	}

void Exploder::UpdateLate()
	{
	}

void Exploder::OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	if ( pOtherCollider->GetParent()->TagEquals("Rectangle") )
		{
		const auto & spRigidBody = wpRigidBody.lock();
		const auto & spSound = wpSound.lock();

		b2Body * pBody = spRigidBody->GetBody();
		b2WorldManifold worldManifold;
		pContact->GetWorldManifold(&worldManifold);
		auto normal = worldManifold.normal;
		normal *= force;
		auto point = worldManifold.points[0];
		pBody->ApplyLinearImpulse(-normal, point, true);
		pOtherCollider->GetFixture()->GetBody()->ApplyLinearImpulse(normal, point, true);
		
		if ( spSound->getStatus() != sf::SoundSource::Status::Playing )
			spSound->play();

		Simple::Scene * pScene = Simple::Game::GetGame()->GetCurrentScene();

		auto spExplosion = pScene->InstantiatePrefab("Explosion.prefab");
		auto spExplosionSprite = spExplosion->GetComponent<Simple::Sprite>().lock();
		spExplosionSprite->setPosition(sf::Vector2f(Simple::Math::MetersToPixels(point.x), Simple::Math::MetersToPixels(point.y)));
		spExplosionSprite->setRotation(Simple::Math::GetRandomBetween(0, 361));
		}
	}

void Exploder::OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider)
	{
	}

Exploder::Exploder()
	:
	Component(),
	force(0)
	{

	}


Exploder::~Exploder()
{
}
