#pragma once
#include "SIMPLE/Game.hpp"

namespace Simple
{
class GameObject;
class Component;
}

using namespace Simple;

namespace Space
{

class Box2D_PhysicsDemo : public Simple::Game
	{
	public:
		Box2D_PhysicsDemo();
		virtual ~Box2D_PhysicsDemo();

		virtual unique_ptr<Component> LoadGameComponent(GameObject * pParent, tinyxml2::XMLElement * pComponentEle, const string & typeString) override;
	};

}