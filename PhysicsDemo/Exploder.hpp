#pragma once
#include "Simple/Component.hpp"
#include <memory>

namespace Simple
{
class RigidBody;
class Sound;
}
class Exploder : public Simple::Component
{
public:
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;
	void OnCollisionEnter(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;
	void OnCollisionExit(b2Contact* pContact, Simple::Collider* pThisCollider, Simple::Collider* pOtherCollider) override;

	explicit Exploder();
	~Exploder();

protected:
	float force;
	weak_ptr<Simple::RigidBody> wpRigidBody;
	weak_ptr<Simple::Sound> wpSound;
};

