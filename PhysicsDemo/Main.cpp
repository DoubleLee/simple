#include <iostream>
#include <string>
#include <exception>
#include "Box2D_PhysicsDemo.hpp"
#include "Simple/Logger.hpp"
int main(char * pArgs, int argCount)
	{
	pArgs;
	argCount;
	try
		{
		Space::Box2D_PhysicsDemo game;
		game.GetLogger().Log("Loading game.");

		game.LoadGameFile("PhysicsDemo.txt");
		
		game.GetLogger().Log("Load Success, beginning loop.");

		game.Run();

		std::string buffer;
		std::getline(std::cin, buffer);
		}
	catch( exception & except )
		{
		string message = except.what();
		throw;
		}
	}