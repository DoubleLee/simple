#pragma once
#include "Simple/Component.hpp"
#include <memory>
#include <SFML/System/Time.hpp>


class DeleteAfterExplosion : public Simple::Component
{
public:
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;
	explicit DeleteAfterExplosion();
	~DeleteAfterExplosion();

	void Constructor() override;
	void Initialize() override;

	void UpdateLate() override;

protected:
	weak_ptr<Simple::AnimatedSprite> wpSprite;
	sf::Time deleteAt;
};

