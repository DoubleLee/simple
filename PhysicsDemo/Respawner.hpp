#pragma once
#include "Simple/Component.hpp"
#include <memory>

namespace Simple
{
class Sprite;
class RigidBody;
class GameObject;
}
class Respawner : public Simple::Component
{
public:
	explicit Respawner();
	~Respawner();
	void Constructor() override;
	void Initialize() override;
	void UpdateEarly() override;
	void Update() override;
	void UpdateLate() override;
	void SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc) override;
	void LoadFromXML(tinyxml2::XMLElement* pComponentEle) override;

protected:
	weak_ptr<Simple::Sprite> wpSprite;
	weak_ptr<Simple::RigidBody> wpRigidBody;
};

