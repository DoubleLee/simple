#include "DeleteAfterExplosion.hpp"

#include "Simple/Engine.hpp"

void DeleteAfterExplosion::SaveToXML(tinyxml2::XMLElement* pComponentEle, tinyxml2::XMLDocument& doc)
	{
	}

void DeleteAfterExplosion::LoadFromXML(tinyxml2::XMLElement* pComponentEle)
	{
	}

DeleteAfterExplosion::DeleteAfterExplosion()
	:
	Simple::Component()
	{
	}

DeleteAfterExplosion::~DeleteAfterExplosion()
	{
	}

void DeleteAfterExplosion::Constructor()
	{
	wpSprite = GetParent()->GetComponent<Simple::AnimatedSprite>();
	}

void DeleteAfterExplosion::Initialize()
	{
	deleteAt = wpSprite.lock()->GetAnimationTimeLength() + Simple::Game::GetTimeSinceGameLoad();
	}

void DeleteAfterExplosion::UpdateLate()
	{
	if ( deleteAt <= Simple::Game::GetTimeSinceGameLoad() )
		GetParent()->SetTaggedForDelete();
	}